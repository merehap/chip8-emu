use std::ops::{Index, IndexMut};

use sdl2::keyboard::Keycode;

use u4::u4;

const LEN: usize = 16;

#[derive(Debug)]
pub struct Keys {
    keys: [bool; 16],
}

impl Keys {
    pub fn new() -> Keys {
        Keys{keys: [false; LEN]}
    }

    pub fn from_sdl_keycodes(keycodes: Vec<Keycode>) -> Keys {
        let mut keys = Keys::new();
        for keycode in keycodes {
            if let Some(k) = key_from_sdl_keycode(keycode) {
                keys[k] = true;
            }
        }

        keys
    }

    pub fn first_pressed(&self) -> Option<u4> {
        for i in 0..LEN {
            if self.keys[i] {
                return Some(u4::from_u16(i as u16).unwrap());
            }
        }

        None
    }
}

pub fn key_from_sdl_keycode(keycode: Keycode) -> Option<u4> {
    use self::Keycode::*;
    use u4::u4::*;
    match keycode {
        Num1 => Some(Ox1),
        Num2 => Some(Ox2),
        Num3 => Some(Ox3),
        Num4 => Some(OxC),
        Q => Some(Ox4),
        W => Some(Ox5),
        E => Some(Ox6),
        R => Some(OxD),
        A => Some(Ox7),
        S => Some(Ox8),
        D => Some(Ox9),
        F => Some(OxE),
        Z => Some(OxA),
        X => Some(Ox0),
        C => Some(OxB),
        V => Some(OxF),
        _ => None,
    }
}

impl Index<u4> for Keys {
    type Output = bool;

    fn index(&self, key_id: u4) -> &bool {
        &self.keys[key_id as usize]
    }
}

impl IndexMut<u4> for Keys {
    fn index_mut(&mut self, key_id: u4) -> &mut bool {
        &mut self.keys[key_id as usize]
    }
}
