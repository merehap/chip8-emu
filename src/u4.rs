use std::fmt;

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
#[allow(non_camel_case_types)]
pub enum u4 {
    Ox0,
    Ox1,
    Ox2,
    Ox3,
    Ox4,
    Ox5,
    Ox6,
    Ox7,
    Ox8,
    Ox9,
    OxA,
    OxB,
    OxC,
    OxD,
    OxE,
    OxF,
}

impl u4 {
    pub fn from_u8(value: u8) -> Option<u4> {
        u4::from_u16(u16::from(value))
    }

    pub fn from_u16(value: u16) -> Option<u4> {
        use u4::u4::*;
        match value {
            0x0 => Some(Ox0),
            0x1 => Some(Ox1),
            0x2 => Some(Ox2),
            0x3 => Some(Ox3),
            0x4 => Some(Ox4),
            0x5 => Some(Ox5),
            0x6 => Some(Ox6),
            0x7 => Some(Ox7),
            0x8 => Some(Ox8),
            0x9 => Some(Ox9),
            0xA => Some(OxA),
            0xB => Some(OxB),
            0xC => Some(OxC),
            0xD => Some(OxD),
            0xE => Some(OxE),
            0xF => Some(OxF),
            _   => None,
        }
    }

    pub fn safe_mul(&self, other: u4) -> u8 {
        (*self as u8).checked_mul(other as u8).expect("Bad int size math.")
    }

    pub fn compose_u8(n0: u4, n1: u4) -> u8 {
        ((n0 as u8) << 4) | (n1 as u8)
    }

    pub fn decompose_u8(value: u8) -> (u4, u4) {
        (u4::from_u8((value & 0xF0) >> 4).expect("Bad bit math"),
         u4::from_u8(value & 0x0F).expect("Bad bit math"))
    }

    pub fn compose_u16(a: u4, b: u4, c: u4, d: u4) -> u16 {
        ((a as u16) << 12) | ((b as u16) << 8) | ((c as u16) << 4) | (d as u16)
    }

    pub fn decompose_u16(value: u16) -> (u4, u4, u4, u4) {
        (u4::from_u16((value & 0xF000) >> 12).expect("Bad bit math"),
         u4::from_u16((value & 0x0F00) >>  8).expect("Bad bit math"),
         u4::from_u16((value & 0x00F0) >>  4).expect("Bad bit math"),
         u4::from_u16(value & 0x000F).expect("Bad bit math"))
    }
}

impl fmt::UpperHex for u4 {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{:X}", *self as u8)
    }
}

impl From<u4> for u16 {
    fn from(value: u4) -> Self {
        value as u16
    }
}

impl <'a> From<&'a u4> for u16 {
    fn from(value: &u4) -> Self {
        *value as u16
    }
}
