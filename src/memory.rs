use std::fmt;
use std::ops::{Index, IndexMut};

use rom::Rom;
use u4::u4;

pub const ROM_START_USIZE: usize = 0x200;

const FONT_SET_SIZE_USIZE: usize = 0x50;
const MEMORY_SIZE_USIZE: usize = 0x1000;

pub struct Memory {
    memory: [u8; MEMORY_SIZE_USIZE],
}

impl Memory {
    pub fn load_rom(rom: &Rom) -> Option<Memory> {
        let mut memory = [0u8; MEMORY_SIZE_USIZE];
        let font_set: [u8; FONT_SET_SIZE_USIZE] = [
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        ];

        memory[..font_set.len()].copy_from_slice(&font_set);
        let rom_bytes = rom.bytes();
        if rom_bytes.len() + ROM_START_USIZE >= memory.len() {
            return None;
        }

        memory[ROM_START_USIZE..rom_bytes.len()+ROM_START_USIZE]
            .copy_from_slice(&rom_bytes[..]);
        Some(Memory{memory})
    }

    pub fn slice(&self, start: Address, len: u4) -> Result<&[u8], String> {
        let (end, overflow) = start.add(u16::from(len) - 1);
        if overflow {
            return Err("Slice may not extend beyond the end of memory.".to_string());
        }

        Ok(&self.memory[usize::from(start)..=usize::from(end)])
    }
}

impl Index<Address> for Memory {
    type Output = u8;

    fn index(&self, address: Address) -> &u8 {
        &self.memory[u16::from(&address) as usize]
    }
}

impl IndexMut<Address> for Memory {
    fn index_mut(&mut self, address: Address) -> &mut u8 {
        &mut self.memory[u16::from(&address) as usize]
    }
}

impl fmt::Debug for Memory {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.memory[..].fmt(f)
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Address {
    high: u4,
    mid: u4,
    low: u4,
}

impl Address {
    pub fn new() -> Address {
        Address {high: u4::Ox0, mid: u4::Ox0, low: u4::Ox0}
    }

    pub fn from_u4s(high: u4, mid: u4, low: u4) -> Address {
        Address { high, mid, low }
    }

    pub fn from_u16(value: u16) -> Option<Address> {
        let (bad, high, mid, low) = u4::decompose_u16(value);
        if bad != u4::Ox0 {
            return None;
        }

        Some(Address {high, mid, low})
    }

    pub fn add(&self, count: u16) -> (Address, bool) {
        let (new_value, u16_overflow) = u16::from(self).overflowing_add(count);
        let (bad, high, mid, low) = u4::decompose_u16(new_value);
        let overflow = u16_overflow || bad != u4::Ox0;
        (Address{high, mid, low}, overflow)
    }
}

impl Default for Address {
    fn default() -> Self {
        Address::new()
    }
}

impl From<u8> for Address {
    fn from(value: u8) -> Address {
        let (mid, low) = u4::decompose_u8(value);
        Address{high: u4::Ox0, mid, low}
    }
}

impl From<Address> for u16 {
    fn from(address: Address) -> Self {
        ((address.high as u16) << 8) | ((address.mid as u16) << 4) | (address.low as u16)
    }
}

impl <'a> From<&'a Address> for u16 {
    fn from(address: &'a Address) -> Self {
        u16::from(*address)
    }
}

impl From<Address> for usize {
    fn from(address: Address) -> Self {
        usize::from(u16::from(address))
    }
}

#[cfg(test)]
mod tests {
    use memory::Address;
    use u4::u4;

    #[test]
    fn roundtrip_u4() {
        let address = Address::from_u4s(u4::OxA, u4::OxB, u4::OxC);
        assert_eq!(0xABC, u16::from(address));
    }

    #[test]
    fn roundtrip_u16() {
        let address = Address::from_u16(0xABC).unwrap();
        assert_eq!(0xABC, u16::from(address));
    }
}
