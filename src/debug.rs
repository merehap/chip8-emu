use memory::ROM_START_USIZE;
use op_code::OpCode;
use rom::Rom;

pub fn dump_rom(rom: &Rom) {
    let bytes = rom.bytes();
    let mut i = 0;
    while i < bytes.len() - 1 {
        let raw_op_code = (u16::from(bytes[i]) << 8) | (u16::from(bytes[i + 1]));
        if let Some(op_code) = OpCode::from_raw_code(raw_op_code) {
            println!("0x{:04X}: 0x{:04X} (0x{:04X}) {:?}",
                     i + ROM_START_USIZE,
                     raw_op_code,
                     op_code.to_raw_code(),
                     op_code);
        } else {
            println!("0x{:04X}: 0x{:04X} UNKNOWN OP CODE (DATA?)", i + ROM_START_USIZE, raw_op_code);
        }

        i += 2;
    }
}