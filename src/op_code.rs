use memory::Address;
use registers::RegisterId;
use u4::u4;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum OpCode {
    ClearScreen,
    EndSubroutine,

    CallProgram(Address),
    CallSubroutine(Address),
    Goto(Address),
    Jump(Address),
    SetIndex(Address),

    SkipOnEqualValue(RegisterId, u8),
    SkipOnNotEqualValue(RegisterId, u8),
    SetValue(RegisterId, u8),
    AddValue(RegisterId, u8),
    Rand(RegisterId, u8),

    SkipOnEqual(RegisterId, RegisterId),
    SkipOnNotEqual(RegisterId, RegisterId),
    Set{dest: RegisterId, source: RegisterId},
    BitOr{dest: RegisterId, source: RegisterId},
    BitAnd{dest: RegisterId, source: RegisterId},
    BitXor{dest: RegisterId, source: RegisterId},
    Add{dest: RegisterId, source: RegisterId},
    Subtract{dest: RegisterId, source: RegisterId},
    RightShift{dest: RegisterId, source: RegisterId},
    ReverseSubtract{dest: RegisterId, source: RegisterId},
    LeftShiftBoth{dest: RegisterId, source: RegisterId},

    Draw{x: RegisterId, y: RegisterId, height: u4},

    SkipOnEqualKey(RegisterId),
    SkipOnNotEqualKey(RegisterId),
    SetFromDelayTimer(RegisterId),
    SetFromKey(RegisterId),
    SetDelayTimer(RegisterId),
    SetSoundTimer(RegisterId),
    AddToIndex(RegisterId),
    SetIndexFromSprite(RegisterId),
    SetBcdAtIndex(RegisterId),
    Dump{last: RegisterId},
    Load{last: RegisterId},
}

impl OpCode {
    pub fn from_raw_code(raw_code: u16) -> Option<OpCode> {
        use u4::u4::*;
        use op_code::OpCode::*;
        Some(match u4::decompose_u16(raw_code) {
            (Ox0, Ox0, OxE, Ox0) => ClearScreen,
            (Ox0, Ox0, OxE, OxE) => EndSubroutine,

            // Address operations.
            (Ox0, a0, a1, a2) => {
                CallProgram(Address::from_u4s(a0, a1, a2))
                //println!("{:?}", CallProgram(Address::from_u4s(a0, a1, a2)));
                //unimplemented!("CallProgram isn't implemented yet.");
            },
            (Ox2, a0, a1, a2) => CallSubroutine(Address::from_u4s(a0, a1, a2)),
            (Ox1, a0, a1, a2) => Goto(Address::from_u4s(a0, a1, a2)),
            (OxB, a0, a1, a2) => Jump(Address::from_u4s(a0, a1, a2)),
            (OxA, a0, a1, a2) => SetIndex(Address::from_u4s(a0, a1, a2)),

            (Ox3, vx, n0, n1) => SkipOnEqualValue(RegisterId::from(vx), u4::compose_u8(n0, n1)),
            (Ox4, vx, n0, n1) => SkipOnNotEqualValue(RegisterId::from(vx), u4::compose_u8(n0, n1)),
            (Ox6, vx, n0, n1) => SetValue(RegisterId::from(vx), u4::compose_u8(n0, n1)),
            (Ox7, vx, n0, n1) => AddValue(RegisterId::from(vx), u4::compose_u8(n0, n1)),
            (OxC, vx, n0, n1) => Rand(RegisterId::from(vx), u4::compose_u8(n0, n1)),

            (Ox5, vx, vy, Ox0) => SkipOnEqual(RegisterId::from(vx), RegisterId::from(vy)),
            (Ox9, vx, vy, Ox0) => SkipOnNotEqual(RegisterId::from(vx), RegisterId::from(vy)),

            // Arithmetic operations.
            (Ox8, vx, vy, sub_code) => {
                let dest = RegisterId::from(vx);
                let source = RegisterId::from(vy);
                match sub_code {
                    Ox0 => Set{dest, source},
                    Ox1 => BitOr{dest, source},
                    Ox2 => BitAnd{dest, source},
                    Ox3 => BitXor{dest, source},
                    Ox4 => Add{dest, source},
                    Ox5 => Subtract{dest, source},
                    Ox6 => RightShift{dest, source},
                    Ox7 => ReverseSubtract{dest, source},
                    OxE => LeftShiftBoth{dest, source},
                    _ => return None,
                }
            },

            (OxD, vx, vy, n0) => Draw{x: RegisterId::from(vx), y: RegisterId::from(vy), height: n0},

            (OxE, vx, Ox9, OxE) => SkipOnEqualKey(RegisterId::from(vx)),
            (OxE, vx, OxA, Ox1) => SkipOnNotEqualKey(RegisterId::from(vx)),

            // Single register operations.
            (OxF, vx, sub_code_0, sub_code_1) => {
                let reg_id = RegisterId::from(vx);
                match (sub_code_0, sub_code_1) {
                    (Ox0, Ox7) => SetFromDelayTimer(reg_id),
                    (Ox0, OxA) => SetFromKey(reg_id),
                    (Ox1, Ox5) => SetDelayTimer(reg_id),
                    (Ox1, Ox8) => SetSoundTimer(reg_id),
                    (Ox1, OxE) => AddToIndex(reg_id),
                    (Ox2, Ox9) => SetIndexFromSprite(reg_id),
                    (Ox3, Ox3) => SetBcdAtIndex(reg_id),
                    (Ox5, Ox5) => Dump{last: reg_id},
                    (Ox6, Ox5) => Load{last: reg_id},
                    _ => return None,
                }
            }

            ( _,  _,  _,  _) => return None,
        })
    }

    pub fn to_raw_code(&self) -> u16 {
        use op_code::OpCode::*;
        use u4::u4::*;
        match self {
            ClearScreen   => u4::compose_u16(Ox0, Ox0, OxE, Ox0),
            EndSubroutine => u4::compose_u16(Ox0, Ox0, OxE, OxE),

            CallProgram(address)    =>               u16::from(address),
            CallSubroutine(address) => (0x2 << 12) | u16::from(address),
            Goto(address)           => (0x1 << 12) | u16::from(address),
            Jump(address)           => (0xB << 12) | u16::from(address),
            SetIndex(address)       => (0xA << 12) | u16::from(address),

            SkipOnEqualValue   (reg_id, value) => (0x3 << 12) | (u16::from(reg_id) << 8) | u16::from(*value),
            SkipOnNotEqualValue(reg_id, value) => (0x4 << 12) | (u16::from(reg_id) << 8) | u16::from(*value),
            SetValue           (reg_id, value) => (0x6 << 12) | (u16::from(reg_id) << 8) | u16::from(*value),
            AddValue           (reg_id, value) => (0x7 << 12) | (u16::from(reg_id) << 8) | u16::from(*value),
            Rand               (reg_id, value) => (0xC << 12) | (u16::from(reg_id) << 8) | u16::from(*value),

            SkipOnEqual   (reg_id_0, reg_id_1) => u4::compose_u16(Ox5, reg_id_0.into(), reg_id_1.into(), Ox0),
            SkipOnNotEqual(reg_id_0, reg_id_1) => u4::compose_u16(Ox9, reg_id_0.into(), reg_id_1.into(), Ox0),

            Set            {dest, source} => u4::compose_u16(Ox8, dest.into(), source.into(), Ox0),
            BitOr          {dest, source} => u4::compose_u16(Ox8, dest.into(), source.into(), Ox1),
            BitAnd         {dest, source} => u4::compose_u16(Ox8, dest.into(), source.into(), Ox2),
            BitXor         {dest, source} => u4::compose_u16(Ox8, dest.into(), source.into(), Ox3),
            Add            {dest, source} => u4::compose_u16(Ox8, dest.into(), source.into(), Ox4),
            Subtract       {dest, source} => u4::compose_u16(Ox8, dest.into(), source.into(), Ox5),
            RightShift     {dest, source} => u4::compose_u16(Ox8, dest.into(), source.into(), Ox6),
            ReverseSubtract{dest, source} => u4::compose_u16(Ox8, dest.into(), source.into(), Ox7),
            LeftShiftBoth  {dest, source} => u4::compose_u16(Ox8, dest.into(), source.into(), OxE),

            Draw{x, y, height} => u4::compose_u16(OxD, x.into(), y.into(), *height),

            SkipOnEqualKey      (reg_id) => u4::compose_u16(OxE, reg_id.into(), Ox9, OxE),
            SkipOnNotEqualKey   (reg_id) => u4::compose_u16(OxE, reg_id.into(), OxA, Ox1),
            SetFromDelayTimer   (reg_id) => u4::compose_u16(OxF, reg_id.into(), Ox0, Ox7),
            SetFromKey          (reg_id) => u4::compose_u16(OxF, reg_id.into(), Ox0, OxA),
            SetDelayTimer       (reg_id) => u4::compose_u16(OxF, reg_id.into(), Ox1, Ox5),
            SetSoundTimer       (reg_id) => u4::compose_u16(OxF, reg_id.into(), Ox1, Ox8),
            AddToIndex          (reg_id) => u4::compose_u16(OxF, reg_id.into(), Ox1, OxE),
            SetIndexFromSprite  (reg_id) => u4::compose_u16(OxF, reg_id.into(), Ox2, Ox9),
            SetBcdAtIndex       (reg_id) => u4::compose_u16(OxF, reg_id.into(), Ox3, Ox3),
            Dump          {last: reg_id} => u4::compose_u16(OxF, reg_id.into(), Ox5, Ox5),
            Load          {last: reg_id} => u4::compose_u16(OxF, reg_id.into(), Ox6, Ox5),
        }
    }
}

#[cfg(test)]
mod tests {
    use op_code::OpCode;
    #[test]
    fn roundtrip() {
        let raw_codes = vec![
            0x00E0,
            0x00EE,
            0x0ABC,
            0x1ABC,
            0x2ABC,
            0x31DE,
            0x41DE,
            0x5120,
            0x61DE,
            0x71DE,
            0x8120,
            0x8121,
            0x8122,
            0x8123,
            0x8124,
            0x8125,
            0x8126,
            0x8127,
            0x812e,
            0x9120,
            0xAABC,
            0xBABC,
            0xC1DE,
            0xD12E,
            0xE19E,
            0xE1A1,
            0xF107,
            0xF10A,
            0xF115,
            0xF118,
            0xF11E,
            0xF129,
            0xF133,
            0xF155,
            0xF165,
        ];

        let codes = raw_codes.iter()
            .map(|&raw| OpCode::from_raw_code(raw).unwrap())
            .collect::<Vec<_>>();
        let result = codes.iter()
            .map(OpCode::to_raw_code)
            .collect::<Vec<_>>();
        assert_eq!(raw_codes, result);
    }

    #[test]
    fn bad_op_code() {
        assert_eq!(None, OpCode::from_raw_code(0xF166));
    }
}
