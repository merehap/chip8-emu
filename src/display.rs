extern crate sdl2;

use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;

use screen;
use screen::Screen;

const SCALE_FACTOR: u16 = 15;

pub struct Display {
    canvas: Canvas<Window>,
    fore_color: Color,
    back_color: Color,
}

impl Display {
    pub fn from_sdl_context(sdl_context: &sdl2::Sdl, game_name: &str) -> Option<Display> {
        let window = sdl_context
            .video().ok()?
            .window(
                &format!("CHIP8: {}", game_name),
                u32::from(screen::WIDTH) * u32::from(SCALE_FACTOR),
                u32::from(screen::HEIGHT) * u32::from(SCALE_FACTOR),
            )
            .position_centered()
            .opengl()
            .build().ok()?;

        let mut canvas = window.into_canvas().build().ok()?;

        let fore_color: Color = Color::RGB(255, 255, 255);
        let back_color: Color = Color::RGB(0, 0, 0);
        canvas.set_draw_color(back_color);
        canvas.clear();

        Some(Display { canvas, fore_color, back_color })
    }

    pub fn draw_screen(&mut self, screen: &Screen) {
        screen.foreach_pixel(&mut |x, y, is_set| {
            let color = if is_set { self.fore_color } else { self.back_color };

            self.canvas.set_draw_color(color);

            let _ = self.canvas.fill_rect(Rect::new(
                i32::from(x) * i32::from(SCALE_FACTOR),
                i32::from(y) * i32::from(SCALE_FACTOR),
                u32::from(SCALE_FACTOR),
                u32::from(SCALE_FACTOR)));
        });

        self.canvas.present();
    }
}
