use std::io::Read;
use std::fs::File;

pub struct Rom {
    rom: Vec<u8>,
}

impl Rom {
    pub fn load(path: &str) -> Option<Rom> {
        let mut file = File::open(path).ok()?;
        let mut bytes = vec![];
        file.read_to_end(&mut bytes).ok()?;

        Some(Rom{rom: bytes})
    }

    pub fn bytes<'a>(&'a self) -> &'a Vec<u8> {
        &self.rom
    }
}
