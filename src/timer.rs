#[derive(Debug)]
pub struct Timer {
    count: u8,
}

impl Timer {
    pub fn new() -> Timer {
        Timer {count: 0}
    }

    pub fn get(&self) -> u8 {
        self.count
    }

    pub fn set(&mut self, count: u8) {
        self.count = count;
    }

    pub fn tick(&mut self) -> bool {
        if self.count == 0 {
            return true;
        }

        self.count -= 1;
        false
    }
}