extern crate sdl2;

use sdl2::EventPump;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

pub struct Events {
    pump: EventPump,
}

impl Events {
    pub fn from_sdl_context(sdl_context: &sdl2::Sdl) -> Result<Events, String> {
        sdl_context.event_pump().map(|pump| Events{pump})
    }

    pub fn should_quit(&mut self) -> bool {
        for event in self.pump.poll_iter() {
            if let Event::Quit { .. } = event {
                return true;
            }
        }

        false
    }

    pub fn pressed_keys(&self) -> Vec<Keycode> {
        self.pump
            .keyboard_state()
            .pressed_scancodes()
            .filter_map(Keycode::from_scancode)
            .collect::<Vec<Keycode>>()
    }
}