extern crate rand;
extern crate sdl2;

mod audio;
mod debug;
mod display;
mod events;
mod keys;
mod memory;
mod op_code;
mod registers;
mod rom;
mod screen;
mod stack;
mod state;
mod timer;
mod u4;

use std::ops::Add;
use std::time::{Duration, Instant};
use std::{thread, time};

use audio::Audio;
use display::Display;
use events::Events;
use keys::Keys;
use rom::Rom;
use state::State;

// Determined through trial and error as the value that makes games most playable.
const PER_CYCLE_DELAY_MS: u64 = 2;
// 60 Hertz
const TIMER_PERIOD_MS: u64 = 1000 / 60;

fn main() {
    let path = "./roms/tetris.rom";
    println!("Loading ROM '{}'.", path);
    let rom = Rom::load(path)
        .expect(&format!("Failed to load ROM from file '{}'.", path));
    debug::dump_rom(&rom);

    println!("Initializing subsystems.");
    let sdl_context = sdl2::init()
        .expect("Failed to initialize SDL.");
    let mut display = Display::from_sdl_context(&sdl_context, path)
        .expect("Failed to load SDL display.");
    let audio = Audio::from_sdl_context(&sdl_context)
        .expect("Failed to load SDL audio.");
    let mut events = Events::from_sdl_context(&sdl_context)
        .expect("Failed to load SDL events.");

    let mut state = State::load_rom(&rom)
        .expect("Failed to load ROM into memory.");
    let mut next_timer_trigger = get_next_timer_trigger(TIMER_PERIOD_MS);
    while !events.should_quit() {
        state.set_keys(Keys::from_sdl_keycodes(events.pressed_keys()));

        if Instant::now() > next_timer_trigger {
            state.decrement_timers();
            next_timer_trigger = get_next_timer_trigger(TIMER_PERIOD_MS);
        }

        let triggers = state.advance();
        if triggers.draw {
            display.draw_screen(state.screen());
        }

        if triggers.beep {
            audio.start_beep();
        } else {
            audio.stop_beep();
        }

        thread::sleep(time::Duration::from_millis(PER_CYCLE_DELAY_MS));
    }

    println!("User ended program.");
}

fn get_next_timer_trigger(period: u64) -> Instant {
    Instant::now().add(Duration::from_millis(period))
}
