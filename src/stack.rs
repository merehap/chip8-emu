use memory::Address;

#[derive(Debug)]
pub struct Stack {
    stack: Vec<Address>,
}

impl Stack {
    pub fn new() -> Stack {
        Stack{stack: vec![]}
    }

    pub fn push(&mut self, address: Address) -> bool {
        if self.stack.len() == 16 {
            return true;
        }

        self.stack.push(address);
        false
    }

    pub fn pop(&mut self) -> Option<Address> {
        self.stack.pop()
    }

    pub fn len(&self) -> usize {
        self.stack.len()
    }
}