use std::fmt;
use std::ops::{Index, IndexMut};

use u4::u4;

const LEN: usize = 16;

pub struct Registers {
    registers: [u8; LEN],
}

impl Registers {
    pub fn new() -> Registers {
        Registers{registers: [0; LEN]}
    }
}

impl Index<RegisterId> for Registers {
    type Output = u8;

    fn index(&self, reg_id: RegisterId) -> &u8 {
        &self.registers[u16::from(&reg_id) as usize]
    }
}

impl IndexMut<RegisterId> for Registers {
    fn index_mut(&mut self, reg_id: RegisterId) -> &mut u8 {
        &mut self.registers[u16::from(&reg_id) as usize]
    }
}

impl fmt::Debug for Registers {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let _ = write!(f, "{{");
        for i in 0..LEN {
            let reg_id = RegisterId::from(u4::from_u16(i as u16).unwrap());
            let _ = write!(f, "{:?}:{:02X},",
                   reg_id,
                   self[reg_id]);
        }

        write!(f, "}}")
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum RegisterId {
    V0,
    V1,
    V2,
    V3,
    V4,
    V5,
    V6,
    V7,
    V8,
    V9,
    VA,
    VB,
    VC,
    VD,
    VE,
    VF,
}

impl RegisterId {
    pub fn from_u16(id: u16) -> Option<Self> {
        Some(RegisterId::from(u4::from_u16(id)?))
    }
}

impl <'a> From<&'a RegisterId> for u16 {
    fn from(reg_id: &RegisterId) -> Self {
        *reg_id as u16
    }
}

impl <'a> From<&'a RegisterId> for u4 {
    fn from(reg_id: &RegisterId) -> Self {
        use registers::RegisterId::*;
        use u4::u4::*;
        match reg_id {
            V0 => Ox0,
            V1 => Ox1,
            V2 => Ox2,
            V3 => Ox3,
            V4 => Ox4,
            V5 => Ox5,
            V6 => Ox6,
            V7 => Ox7,
            V8 => Ox8,
            V9 => Ox9,
            VA => OxA,
            VB => OxB,
            VC => OxC,
            VD => OxD,
            VE => OxE,
            VF => OxF,
        }
    }
}

impl From<u4> for RegisterId {
    fn from(id: u4) -> Self {
        use u4::u4::*;
        use registers::RegisterId::*;
        match id {
            Ox0 => V0,
            Ox1 => V1,
            Ox2 => V2,
            Ox3 => V3,
            Ox4 => V4,
            Ox5 => V5,
            Ox6 => V6,
            Ox7 => V7,
            Ox8 => V8,
            Ox9 => V9,
            OxA => VA,
            OxB => VB,
            OxC => VC,
            OxD => VD,
            OxE => VE,
            OxF => VF,
        }
    }
}
