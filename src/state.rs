extern crate rand;

use keys::Keys;
use memory::{Address, Memory};
use op_code::OpCode;
use registers::{RegisterId, Registers};
use rom::Rom;
use screen::{Screen, Sprite};
use stack::Stack;
use timer::Timer;
use u4::u4;

#[derive(Debug)]
pub struct State {
    memory: Memory,
    registers: Registers,
    index_register: Address,
    program_counter: Address,
    screen: Screen,
    delay_timer: Timer,
    sound_timer: Timer,
    stack: Stack,
    keys: Keys,
    key_wait_reg_id: Option<RegisterId>,
}

impl State {
    pub fn load_rom(rom: &Rom) -> Option<State> {
        Some(State {
            memory: Memory::load_rom(rom)?,
            registers: Registers::new(),
            index_register: Address::new(),
            program_counter: Address::from_u4s(u4::Ox2, u4::Ox0, u4::Ox0),
            screen: Screen::new(),
            delay_timer: Timer::new(),
            sound_timer: Timer::new(),
            stack: Stack::new(),
            keys: Keys::new(),
            key_wait_reg_id: None,
        })
    }

    pub fn screen(&self) -> &Screen {
        &self.screen
    }

    pub fn set_keys(&mut self, keys: Keys) {
        self.keys = keys;
    }

    pub fn decrement_timers(&mut self) {
        self.delay_timer.tick();
        self.sound_timer.tick();
    }

    pub fn advance(&mut self) -> Triggers {
        let mut triggers = Triggers::new();

        if let Some(reg_id) = self.key_wait_reg_id {
            if let Some(key) = self.keys.first_pressed() {
                self.registers[reg_id] = key as u8;
                self.key_wait_reg_id = None;
            } else {
                // Continue to wait for a key press, don't execute the next op code.
                return triggers;
            }
        }

        if self.sound_timer.get() > 0 {
            triggers.beep = true;
        }

        let op_code = self.get_next_op_code();

        let (program_counter_action, draw) = self.execute_op_code(op_code);
        triggers.draw = draw;

        let (new_program_counter, overflow) = match program_counter_action {
            ProgramCounterAction::Next => self.program_counter.add(2),
            ProgramCounterAction::Skip => self.program_counter.add(4),
            ProgramCounterAction::Jump(address) => (address, false),
        };

        if overflow {
            panic!("Program counter overflowed!");
        }

        self.program_counter = new_program_counter;

        triggers
    }

    fn get_next_op_code(&self) -> OpCode {
        let bytes = self.memory.slice(self.program_counter, u4::Ox2)
            .expect("Failed to extract op code bytes from memory.");
        let raw_op_code = ((u16::from(bytes[0])) << 8) | u16::from(bytes[1]);
        let op_code = OpCode::from_raw_code(raw_op_code)
            .expect(&format!("Unknown op code provided: 0x{:04X}", raw_op_code));
        let indent = "\t".repeat(self.stack.len() + 1);
        println!("0x{:04X}: {:?} 0x{:04X} {}{:?}",
            u16::from(self.program_counter),
            self.registers,
            raw_op_code,
            indent,
            op_code);

        op_code
    }

    fn execute_op_code(&mut self, op_code: OpCode) -> (ProgramCounterAction, bool) {
        use op_code::OpCode::*;
        let mut draw = false;
        let action = match op_code {
            ClearScreen => {
                draw = true;
                self.clear_screen()
            },
            EndSubroutine => self.end_subroutine(),

            CallProgram(address)    => self.call_program(address),
            CallSubroutine(address) => self.call_subroutine(address),
            Goto(address)           => self.goto(address),
            Jump(address)           => self.jump(address),
            SetIndex(address)       => self.set_index(address),

            SkipOnEqualValue   (reg_id, value) => self.skip_on_equal_value(reg_id, value),
            SkipOnNotEqualValue(reg_id, value) => self.skip_on_not_equal_value(reg_id, value),
            SetValue           (reg_id, value) => self.set_value(reg_id, value),
            AddValue           (reg_id, value) => self.add_value(reg_id, value),
            Rand               (reg_id, value) => self.rand(reg_id, value),

            SkipOnEqual   (reg_id_0, reg_id_1) => self.skip_on_equal(reg_id_0, reg_id_1),
            SkipOnNotEqual(reg_id_0, reg_id_1) => self.skip_on_not_equal(reg_id_0, reg_id_1),

            Set            {dest, source} => self.set(dest, source),
            BitOr          {dest, source} => self.bit_or(dest, source),
            BitAnd         {dest, source} => self.bit_and(dest, source),
            BitXor         {dest, source} => self.bit_xor(dest, source),
            Add            {dest, source} => self.add(dest, source),
            Subtract       {dest, source} => self.subtract(dest, source),
            RightShift     {dest, source} => self.right_shift(dest, source),
            ReverseSubtract{dest, source} => self.reverse_subtract(dest, source),
            LeftShiftBoth  {dest, source} => self.left_shift_both(dest, source),

            Draw{x, y, height} => {
                draw = true;
                self.draw(x, y, height)
            },

            SkipOnEqualKey      (reg_id) => self.skip_on_equal_key(reg_id),
            SkipOnNotEqualKey   (reg_id) => self.skip_on_not_equal_key(reg_id),
            SetFromDelayTimer   (reg_id) => self.set_from_delay_timer(reg_id),
            SetFromKey          (reg_id) => self.set_from_key(reg_id),
            SetDelayTimer       (reg_id) => self.set_delay_timer(reg_id),
            SetSoundTimer       (reg_id) => self.set_sound_timer(reg_id),
            AddToIndex          (reg_id) => self.add_to_address(reg_id),
            SetIndexFromSprite  (reg_id) => self.set_index_from_sprite(reg_id),
            SetBcdAtIndex       (reg_id) => self.set_bcd_at_address(reg_id),
            Dump          {last: reg_id} => self.dump(reg_id),
            Load          {last: reg_id} => self.load(reg_id),
        };

        (action, draw)
    }

    fn clear_screen(&mut self) -> ProgramCounterAction {
        self.screen.clear();
        ProgramCounterAction::Next
    }

    fn end_subroutine(&mut self) -> ProgramCounterAction {
        let program_counter = self.stack.pop()
            .expect("Can't end subroutine while not in a subroutine.");
        ProgramCounterAction::Jump(program_counter)
    }

    fn call_program(&mut self, _address: Address) -> ProgramCounterAction {
        // This op code doesn't seem to be used and it isn't clear how to implement it.
        ProgramCounterAction::Next
    }

    fn call_subroutine(&mut self, address: Address) -> ProgramCounterAction {
        let (after_subroutine_address, overflow) = self.program_counter.add(2);
        if overflow {
            panic!("Program counter would overflow after return from subroutine.");
        }

        let overflow = self.stack.push(after_subroutine_address);
        if overflow {
            panic!("Too many nested subroutines called!");
        }

        ProgramCounterAction::Jump(address)
    }

    fn goto(&mut self, address: Address) -> ProgramCounterAction {
        ProgramCounterAction::Jump(address)
    }

    fn jump(&mut self, address: Address) -> ProgramCounterAction {
        let (program_counter, overflow) = address.add(u16::from(self.registers[RegisterId::V0]));
        if overflow {
            panic!("Attempted to jump past accessible memory.");
        }

        ProgramCounterAction::Jump(program_counter)
    }

    fn set_index(&mut self, address: Address) -> ProgramCounterAction {
        self.index_register = address;
        ProgramCounterAction::Next
    }

    fn skip_on_equal_value(&mut self, reg_id: RegisterId, value: u8) -> ProgramCounterAction {
        if self.registers[reg_id] == value {
            ProgramCounterAction::Skip
        } else {
            ProgramCounterAction::Next
        }
    }

    fn skip_on_not_equal_value(&mut self, reg_id: RegisterId, value: u8) -> ProgramCounterAction {
        if self.registers[reg_id] == value {
            ProgramCounterAction::Next
        } else {
            ProgramCounterAction::Skip
        }
    }

    fn set_value(&mut self, reg_id: RegisterId, value: u8) -> ProgramCounterAction {
        self.registers[reg_id] = value;
        ProgramCounterAction::Next
    }

    fn add_value(&mut self, reg_id: RegisterId, value: u8) -> ProgramCounterAction {
        self.registers[reg_id] = self.registers[reg_id].wrapping_add(value);
        ProgramCounterAction::Next
    }

    fn rand(&mut self, reg_id: RegisterId, value: u8) -> ProgramCounterAction {
        self.registers[reg_id] = rand::random::<u8>() & value;
        ProgramCounterAction::Next
    }

    fn skip_on_equal(&mut self, reg_id_0: RegisterId, reg_id_1: RegisterId) -> ProgramCounterAction {
        if self.registers[reg_id_0] == self.registers[reg_id_1] {
            ProgramCounterAction::Skip
        } else {
            ProgramCounterAction::Next
        }
    }

    fn skip_on_not_equal(&mut self, reg_id_0: RegisterId, reg_id_1: RegisterId) -> ProgramCounterAction {
        if self.registers[reg_id_0] == self.registers[reg_id_1] {
            ProgramCounterAction::Next
        } else {
            ProgramCounterAction::Skip
        }
    }

    fn set(&mut self, dest: RegisterId, source: RegisterId) -> ProgramCounterAction {
        self.registers[dest] = self.registers[source];
        ProgramCounterAction::Next
    }

    fn bit_or(&mut self, dest: RegisterId, source: RegisterId) -> ProgramCounterAction {
        self.registers[dest] |= self.registers[source];
        ProgramCounterAction::Next
    }

    fn bit_and(&mut self, dest: RegisterId, source: RegisterId) -> ProgramCounterAction {
        self.registers[dest] &= self.registers[source];
        ProgramCounterAction::Next
    }

    fn bit_xor(&mut self, dest: RegisterId, source: RegisterId) -> ProgramCounterAction {
        self.registers[dest] ^= self.registers[source];
        ProgramCounterAction::Next
    }

    fn add(&mut self, dest: RegisterId, source: RegisterId) -> ProgramCounterAction {
        let (result, overflowed) = self.registers[dest].overflowing_add(self.registers[source]);
        self.registers[dest] = result;
        self.registers[RegisterId::VF] = if overflowed { 1 } else { 0 };
        ProgramCounterAction::Next
    }

    fn subtract(&mut self, dest: RegisterId, source: RegisterId) -> ProgramCounterAction {
        let (result, overflowed) = self.registers[dest].overflowing_sub(self.registers[source]);
        self.registers[dest] = result;
        self.registers[RegisterId::VF] = if overflowed { 0 } else { 1 };
        ProgramCounterAction::Next
    }

    fn reverse_subtract(&mut self, dest: RegisterId, source: RegisterId) -> ProgramCounterAction {
        let (result, overflowed) = self.registers[source].overflowing_sub(self.registers[dest]);
        self.registers[dest] = result;
        self.registers[RegisterId::VF] = if overflowed { 0 } else { 1 };
        ProgramCounterAction::Next
    }

    fn right_shift(&mut self, dest: RegisterId, source: RegisterId) -> ProgramCounterAction {
        let low_bit = self.registers[source] & 1;
        self.registers[dest] = self.registers[source] >> 1;
        self.registers[RegisterId::VF] = low_bit;
        ProgramCounterAction::Next
    }

    fn left_shift_both(&mut self, dest: RegisterId, source: RegisterId) -> ProgramCounterAction {
        let high_bit = self.registers[source] >> 7;
        self.registers[source] <<= 1;
        self.registers[dest] = self.registers[source];
        self.registers[RegisterId::VF] = high_bit;
        ProgramCounterAction::Next
    }

    fn draw(&mut self, x_id: RegisterId, y_id: RegisterId, height: u4) -> ProgramCounterAction {
        let sprite_bytes = self.memory.slice(self.index_register, height)
            .expect("Failed to extract sprite bytes from memory.");
        let sprite = Sprite::from_bytes(sprite_bytes)
            .expect("Sprite exceeded maximum size");
        let collided = self.screen.draw_sprite(
            self.registers[x_id],
            self.registers[y_id],
            &sprite);

        self.registers[RegisterId::VF] = if collided { 1 } else { 0 };
        ProgramCounterAction::Next
    }

    fn skip_on_equal_key(&mut self, reg_id: RegisterId) -> ProgramCounterAction {
        let key_id = self.registers[reg_id];
        if let Some(id) = u4::from_u16(u16::from(key_id)) {
            if self.keys[id] {
                ProgramCounterAction::Skip
            } else {
                ProgramCounterAction::Next
            }
        } else {
            panic!("Illegal key ID of {:?} in {:?}.", key_id, reg_id);
        }
    }

    fn skip_on_not_equal_key(&mut self, reg_id: RegisterId) -> ProgramCounterAction {
        let key_id = self.registers[reg_id];
        if let Some(id) = u4::from_u16(u16::from(key_id)) {
            if self.keys[id] {
                ProgramCounterAction::Next
            } else {
                ProgramCounterAction::Skip
            }
        } else {
            panic!("Illegal key ID of {:?} in {:?}.", key_id, reg_id);
        }
    }

    fn set_from_delay_timer(&mut self, reg_id: RegisterId) -> ProgramCounterAction {
        self.registers[reg_id] = self.delay_timer.get();
        ProgramCounterAction::Next
    }

    fn set_from_key(&mut self, reg_id: RegisterId) -> ProgramCounterAction {
        self.key_wait_reg_id = Some(reg_id);
        ProgramCounterAction::Next
    }

    fn set_delay_timer(&mut self, reg_id: RegisterId) -> ProgramCounterAction {
        self.delay_timer.set(self.registers[reg_id]);
        ProgramCounterAction::Next
    }

    fn set_sound_timer(&mut self, reg_id: RegisterId) -> ProgramCounterAction {
        self.sound_timer.set(self.registers[reg_id]);
        ProgramCounterAction::Next
    }

    fn add_to_address(&mut self, reg_id: RegisterId) -> ProgramCounterAction {
        let value = u16::from(self.registers[reg_id]);
        self.advance_index_with_overflow(value);
        ProgramCounterAction::Next
    }

    fn set_index_from_sprite(&mut self, reg_id: RegisterId) -> ProgramCounterAction {
        let value = self.registers[reg_id];
        if let Some(sprite_id) = u4::from_u8(value) {
            self.index_register = sprite_id.safe_mul(u4::Ox5).into();
        } else {
            panic!("Illegal sprite ID of {:?} in {:?}.", value, reg_id);
        }

        ProgramCounterAction::Next
    }

    fn set_bcd_at_address(&mut self, reg_id: RegisterId) -> ProgramCounterAction {
        let mut value = self.registers[reg_id];
        for &offset in &[2, 1, 0] {
            self.memory[self.index_register.add(offset).0] = value % 10;
            value /= 10;
        }

        ProgramCounterAction::Next
    }

    fn dump(&mut self, last_reg_id: RegisterId) -> ProgramCounterAction {
        let mut address = self.index_register;
        for id in 0..=last_reg_id as u16 {
            let reg_id = RegisterId::from_u16(id).expect("Bad register ID");
            self.memory[address] = self.registers[reg_id];

            let (new_address, overflow) = address.add(1);
            if overflow {
                panic!("Attempted to write beyond memory bounds during register dump.");
            }

            address = new_address;
        }

        ProgramCounterAction::Next
    }

    fn load(&mut self, last_reg_id: RegisterId) -> ProgramCounterAction {
        let mut address = self.index_register;
        for id in 0..=last_reg_id as u16 {
            let reg_id = RegisterId::from_u16(id).expect("Bad register ID");
            self.registers[reg_id] = self.memory[address];

            let (new_address, overflow) = address.add(1);
            if overflow {
                panic!("Attempted to read beyond memory bounds during register load.");
            }

            address = new_address;
        }

        ProgramCounterAction::Next
    }

    fn advance_index_with_overflow(&mut self, count: u16) {
        let (new_address, overflow) = self.index_register.add(count);
        self.index_register = new_address;
        self.registers[RegisterId::VF] = if overflow { 1 } else { 0 };
    }
}

enum ProgramCounterAction {
    Next,
    Skip,
    Jump(Address),
}

pub struct Triggers {
    pub draw: bool,
    pub beep: bool,
}

impl Triggers {
    pub fn new() -> Triggers {
        Triggers{draw: false, beep: false}
    }
}