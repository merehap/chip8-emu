use std::fmt;

pub const WIDTH: u16 = 64;
pub const HEIGHT: u16 = 32;

pub struct Screen {
    screen: [[bool; HEIGHT as usize]; WIDTH as usize],
}

impl Screen {
    pub fn new() -> Screen {
        Screen{screen: [[false; HEIGHT as usize]; WIDTH as usize]}
    }

    pub fn draw_sprite(&mut self, x: u8, y: u8, sprite: &Sprite) -> bool {
        let mut collided = false;
        for y_offset in 0..sprite.bytes().len() as u8 {
            let row = u8_to_bools(sprite.bytes()[usize::from(y_offset)]);
            for x_offset in 0..row.len() as u8 {
                let collision = self.draw_pixel(
                    safe_add(x, x_offset),
                    safe_add(y, y_offset),
                    row[x_offset as usize]);
                if collision {
                    collided = true;
                }
            }
        }

        collided
    }

    pub fn clear(&mut self) {
        for x in 0..WIDTH {
            for y in 0..HEIGHT {
                self.screen[x as usize][y as usize] = false;
            }
        }
    }

    pub fn foreach_pixel(&self, f: &mut FnMut(u16, u16, bool)) {
        for x in 0..WIDTH {
            for y in 0..HEIGHT {
                f(x, y, self.screen[x as usize][y as usize]);
            }
        }
    }

    fn draw_pixel(&mut self, x: u16, y: u16, on: bool) -> bool {
        if x >= WIDTH || y >= HEIGHT {
            // Do nothing if the draw would be off the screen.
            return false;
        }

        let original = self.screen[x as usize][y as usize];
        self.screen[x as usize][y as usize] ^= &on;
        // Indicate whether a collision occurred.
        original && on
    }
}

impl fmt::Debug for Screen {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut result = Result::Ok(());
        for column in self.screen.iter() {
            result = column[..].fmt(f)
        }

        result
    }
}

fn u8_to_bools(value: u8) -> [bool; 8] {
    let mut bools = [false; 8];
    let mut mask = 0b1000_0000;
    for b in &mut bools {
        *b = value & mask != 0;
        mask >>= 1;
    }

    bools
}

fn safe_add(val0: u8, val1: u8) -> u16 {
    u16::from(val0).checked_add(u16::from(val1)).expect("Int size bug.")
}

pub struct Sprite<'a> {
    sprite: &'a [u8],
}

impl <'a> Sprite<'a> {
    const MIN_HEIGHT: usize = 1;
    const MAX_HEIGHT: usize = 15;
    pub fn from_bytes(bytes: &'a [u8]) -> Result<Sprite<'a>, String> {
        if bytes.len() < Sprite::MIN_HEIGHT || bytes.len() > Sprite::MAX_HEIGHT {
            return Err(format!(
                "Height must not be less than {} nor greater than {}.",
                Sprite::MIN_HEIGHT,
                Sprite::MAX_HEIGHT));
        }

        Ok(Sprite{sprite: bytes})
    }

    pub fn bytes(&'a self) -> &'a [u8] {
        self.sprite
    }
}
